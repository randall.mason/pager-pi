#!/bin/bash -e

hostname=$(/usr/bin/basename "$(cat /boot/signage.txt)" | /bin/sed -e 's/[- ]//g')
echo ${hostname,,}
/bin/grep ${hostname,,} /etc/hostname || {
/usr/bin/augtool set /files/etc/hostname/hostname ${hostname,,}
/usr/bin/augtool set /files/etc/hosts/5/canonical ${hostname,,}

/sbin/fatlabel /dev/mmcblk0p1 ${hostname,,}

/bin/hostname -F /etc/hostname

/sbin/reboot
}
