---
- name: Find cloudflare domain
  command:
    cmd: bash -c "journalctl -xe -u cloudflared.service | grep -iE -o 'https://[A-Za-z.-]*trycloudflare.com' | tail -1"
  register: cloudflared_domain    
  check_mode: no

- name: Check Uptimerobot Contact
  uri:
    url: "https://api.uptimerobot.com/v2/getAlertContacts"
    method: POST
    return_content: yes
    body_format: json
    body:
      api_key: "{{ uptimerobot_api_key }}"
    follow_redirects: all
  register: alert_contacts
  failed_when: '"fail" == alert_contacts.json.stat'
  check_mode: no

- name: Add Uptimerobot Contact
  uri:
    url: "https://api.uptimerobot.com/v2/newAlertContact"
    method: POST
    return_content: yes
    body_format: json
    body:
      api_key: "{{ uptimerobot_api_key }}"
      friendly_name: "{{ alert_slug }}"
      value: "{{ ifttt_webhook_url }}"
      type: "5"
      status: "2"
    follow_redirects: all
  register: add_contact_webhook
  failed_when: '"fail" == add_contact_webhook.json.stat'
  when: alert_slug not in contacts
  vars:
    contacts:
      "{{ alert_contacts | json_query('json.alert_contacts[*].friendly_name')}}"

- name: Pick right Contact
  set_fact:
    alert_contact: "{{ alert_contacts | json_query(query) | first}}"
  when:
    - add_contact_webhook is skipped
  vars:
    query:
      "json.alert_contacts[?friendly_name=='{{ alert_slug }}'].id"

- name: Pick right Contact
  set_fact:
    alert_contact: "{{ add_contact_webhook | json_query(query) }}"
  when: not add_contact_webhook is skipped
  vars:
    query:
      "json.alertcontact.id"

- name: Check Uptimerobot Monitoring
  uri:
    url: "https://api.uptimerobot.com/v2/getMonitors"
    method: POST
    return_content: yes
    body_format: json
    body:
      api_key: "{{ uptimerobot_api_key }}"
    follow_redirects: all
  register: current_monitors
  failed_when: '"fail" == current_monitors.json.stat'
  check_mode: no

- name: Add Uptimerobot Monitor
  uri:
    url: "https://api.uptimerobot.com/v2/newMonitor"
    method: POST
    return_content: yes
    body_format: json
    body:
      api_key: "{{ uptimerobot_api_key }}"
      friendly_name: "{{ alert_slug }}"
      url: "{{ cloudflared_domain.stdout }}"
      type: "1"
      alert_contacts: "{{ alert_contact }}_0_0"
    follow_redirects: all
  register: new_monitor
  failed_when: '"fail" == new_monitor.json.stat'
  when: alert_slug not in monitors
  vars:
    monitors:
      "{{ current_monitors | json_query('json.monitors[*].friendly_name')}}"

- name: Edit Uptimerobot Monitor
  uri:
    url: "https://api.uptimerobot.com/v2/editMonitor"
    method: POST
    return_content: yes
    body_format: json
    body:
      api_key: "{{ uptimerobot_api_key }}"
      id: "{{ monitor }}"
      url: "{{ cloudflared_domain.stdout }}"
      alert_contacts: "{{ alert_contact }}_0_0"
    follow_redirects: all
  register: edit_monitor
  failed_when: '"fail" == edit_monitor.json.stat'
  when:
    - new_monitor is skipped
    - cloudflared_domain.stdout not in current_url
  vars:
    monitor:
      "{{ current_monitors | json_query(id_query) | first }}"
    current_url:
      "{{ current_monitors | json_query(url_query) | first }}"
    id_query:
      "{{ query }}.id"
    url_query:
      "{{ query }}.url"
    query:
      "json.monitors[?friendly_name=='{{ alert_slug }}']"

# alertcontact>type	
# 1 - SMS
# 2 - E-mail
# 3 - Twitter DM
# 4 - Boxcar
# 5 - Web-Hook
# 6 - Pushbullet
# 7 - Zapier
# 9 - Pushover
# 10 - HipChat
# 11 - Slack

# alertcontact>status	
# 0 - not activated
# 1 - paused
# 2 - active
